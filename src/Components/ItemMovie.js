import React from 'react'
import { Card, Icon } from 'antd'
import TextTruncate from 'react-text-truncate'

const Meta = Card.Meta;

function ItemMovie(props) {
    const item = props.item;
    var TextTruncate = require('react-text-truncate')
    return (

        <Card
            cover={<img alt="example" src={item.image_url} />}
            onClick = {()=>{props.onItemeMovieClick(item.title)}}
        >

            <Meta
                title={item.title}

                description={
                    <TextTruncate
                        line={1}
                        truncateText="…"
                        text={item.overview}
                        textTruncateChild={<a href= "#" >Read on</a>}
                    />
                }
            />


        </Card>

    )
}

export default ItemMovie;