import React from "react";
import { Component } from 'react'
import { Spin, Button, Modal } from 'antd';
import ListMovie from '../Components/ListMovie'
import ItemMovie from "../Components/ItemMovie";

class Movies extends Component {

    state = {
        items: [],
        isShowModal: false,
        itemMovie: null
    }
    onItemeMovieClick = (items) => {
        this.setState({ isShowModal: true })
    }

    onModalClickCancle() { this.setState({ isShowModal: false }) }
    onModalClickOk() { this.setState({ isShowModal: false }) }

    componentDidMount() {

        fetch('https://workshopup.herokuapp.com/movie')
            .then(response => response.json())
            .then(object => this.setState({ items: object.results }))
    }

    render() {
        return (
            <div >
                {this.state.items.length > 0 ? (
                    <div>
                        <ListMovie items={this.state.items} onItemeMovieClick={this.onItemeMovieClick} />
                    </div>
                ) : (
                        <Spin size="large" />
                    )}
                {}
                <Modal
                    title="Basic Modal"
                    visible={this.state.isShowModal}
                    onOk={this.onModalClickOk}
                    onCancel={this.onModalClickCancle}
                >
                    <p>Some contents...</p>
                    <p>Some contents...</p>
                    <p>Some contents...</p>
                </Modal>

            </div>
        )
    }
}



export default Movies;