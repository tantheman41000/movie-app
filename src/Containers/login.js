import React, { Component } from 'react';
import { Form, Button, Input, Icon, message } from 'antd';
import Password from 'antd/lib/input/Password';
import {withRouter} from 'react-router-dom'

class Login extends Component {
    state = {
        email: "",
        password: ""
    }

    onEmailChange = (event) => {
        const email = event.target.value
        this.setState({ email })

    }
    onPasswordChange = (event) => {
        const password = event.target.value
        this.setState({ password })
    }

    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    validatePassword(password) {
        var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
        return re.test(String(password));
    }

    handleSubmit = (e) => {
        const {history} = this.props
        e.preventDefault()
        const isValid = this.validateEmail(this.state.email);
        const IsvalidPassword = this.validatePassword(this.state.password)
        if (isValid && IsvalidPassword) {
            history.push('/Movies') //desire path

        } else {
            //TODO: handle something with string
            console.log("invalid")
            message.error('email or password invalid', 1)
        }
    }

    render() {
        return (
            <div style = {{width: "30%"}} >
                <h2>Login</h2>
                <Form onSubmit={this.handleSubmit}>
                    <Form.Item>
                        <Input
                            prefix={<Icon type="user" />}
                            placeholder="Email"
                            onChange={this.onEmailChange}
                        />
                    </Form.Item>
                    <Form.Item>
                        <Input
                            prefix={<Icon type="heat-map" />}
                            type="password"
                            placeholder="Password"
                            onChange={this.onPasswordChange}
                        />
                    </Form.Item>

                    <Form.Item>
                        <Button htmlType="submit">Log in</Button>
                    </Form.Item>
                </Form>

            </div>

        );
    }
}

export default withRouter(Login);
