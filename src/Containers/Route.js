import React  from 'react'
import {Route} from 'react-router-dom'
import Login from './login'
import Movies from './movies';

function Routes() {
    return(
        <div>
        < Route  exact path="/" component= {Login} />
        < Route  exact path="/movies" component= {Movies} />
        </div>
    )
}

export default Routes;
